package com.demo.orders.controllers;

import com.demo.orders.dto.Order;
import com.demo.orders.dto.OrderUpdate;
import com.demo.orders.services.DatabaseCommunicationException;
import com.demo.orders.services.OrdersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/orders")
public class OrdersController {

	@Autowired
	private OrdersService ordersService;

	@RequestMapping(value = "/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Order>> getOrders() {

		try {
			List<Order> orderList = ordersService.getOrdersList();
			return new ResponseEntity<List<Order>>(orderList, HttpStatus.OK);
		} catch (DatabaseCommunicationException e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
					.header("Error-Id", e.getIdentifier())
					.header("Error-Message", e.getMessage()).build();
		}
	}

	@RequestMapping(value = "/", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Void> updateOrdersStatus(@RequestBody List<OrderUpdate> orderUpdates) {

		try {
			ordersService.updateOrders(orderUpdates);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (DatabaseCommunicationException e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
					.header("Error-Id", e.getIdentifier())
					.header("Error-Message", e.getMessage()).build();
		}
	}
}
