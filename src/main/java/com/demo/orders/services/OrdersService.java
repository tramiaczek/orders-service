package com.demo.orders.services;

import com.demo.orders.dto.Order;
import com.demo.orders.dto.OrderUpdate;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class OrdersService {

	private static final String QUERY_GET_ORDERS = "select id, name, description, finished from orders";

	private static final String QUERY_MARK_AS_FINISHED = "update orders set finished=? where id=?";

	private static final Log LOG = LogFactory.getLog(OrdersService.class);

	@Autowired
	@Qualifier("ordersDataSource")
	private DataSource dataSource;

	public List<Order> getOrdersList() throws DatabaseCommunicationException {

		List<Order> ordersFromDatabase = new ArrayList<>();
		Connection connection = null;
		PreparedStatement preparedStatement = null;

		try {
			connection = dataSource.getConnection();
			preparedStatement = connection.prepareStatement(QUERY_GET_ORDERS);
			ResultSet ordersFromDatabaseResultSet = preparedStatement.executeQuery();

			while (ordersFromDatabaseResultSet.next()) {
				int orderIdCol = ordersFromDatabaseResultSet.getInt("id");
				String orderNameCol = ordersFromDatabaseResultSet.getString("name");
				String orderDescriptionCol = ordersFromDatabaseResultSet.getString("description");
				String orderFinishedCol = ordersFromDatabaseResultSet.getString("finished");
				boolean orderFinished = false;

				if ("Y".equalsIgnoreCase(orderFinishedCol)) {
					orderFinished = true;
				}

				ordersFromDatabase.add(new Order(orderIdCol, orderNameCol, orderDescriptionCol, orderFinished));
			}

		} catch (SQLException e) {
			logException(e, true);
		} finally {

			try {
				if (preparedStatement != null) {
					preparedStatement.close();
				}

				if (connection != null) {
					connection.close();
				}
			} catch (SQLException e) {
				logException(e, false);
			}
		}

		return ordersFromDatabase;
	}

	public void updateOrders(List<OrderUpdate> orderUpdateItems) throws DatabaseCommunicationException {

		Connection connection = null;

		try {
			connection = dataSource.getConnection();

			for (OrderUpdate orderUpdate : orderUpdateItems) {
				PreparedStatement preparedStatement = connection.prepareStatement(QUERY_MARK_AS_FINISHED);
				preparedStatement.setInt(2, orderUpdate.getId());
				preparedStatement.setString(1, (orderUpdate.isFinished() ? "Y" : "N"));
				preparedStatement.execute();
				preparedStatement.close();
			}

		} catch (SQLException e) {
			logException(e, true);
		} finally {

			try {
				if (connection != null) {
					connection.close();
				}
			} catch (SQLException e) {
				logException(e, false);
			}
		}
	}

	private void logException(SQLException e, boolean throwException) throws DatabaseCommunicationException {
		String identifier = UUID.randomUUID().toString();
		LOG.error(String.format("[Error-ID: %s] Some unexpected problem occurred: ", identifier), e);
		if (throwException) {
			throw new DatabaseCommunicationException(e.getMessage(), identifier);
		}
	}
}
