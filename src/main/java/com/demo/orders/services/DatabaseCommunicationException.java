package com.demo.orders.services;

public class DatabaseCommunicationException extends Exception {

	private String message;
	private String identifier;

	public DatabaseCommunicationException(String message, String identifier) {
		this.identifier = identifier;
		this.message = message;
	}

	@Override
	public String getMessage() {
		return message;
	}

	public String getIdentifier() {
		return identifier;
	}
}
