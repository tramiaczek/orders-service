package com.demo.orders;

import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;


@Configuration
public class DatabaseConfiguration {

	@Bean
	@ConfigurationProperties("spring.datasource")
	public DataSource ordersDataSource() {
		return DataSourceBuilder.create().build();
	}

}
