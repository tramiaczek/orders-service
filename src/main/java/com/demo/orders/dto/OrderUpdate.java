package com.demo.orders.dto;

import java.io.Serializable;

public class OrderUpdate implements Serializable {

	private int id;

	private boolean finished;

	public OrderUpdate() {

	}

	public OrderUpdate(int id, boolean finished) {
		this.id = id;
		this.finished = finished;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public boolean isFinished() {
		return finished;
	}

	public void setFinished(boolean finished) {
		this.finished = finished;
	}
}
