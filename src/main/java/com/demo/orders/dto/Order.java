package com.demo.orders.dto;

import java.io.Serializable;

public class Order implements Serializable {

	private int id;

	private String name;

	private String description;

	private boolean finished;

	public Order() {

	}

	public Order(int id, String name, String description, boolean finished) {
		this.id = id;
		this.name = name;
		this.description = description;
		this.finished = finished;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isFinished() {
		return finished;
	}

	public void setFinished(boolean finished) {
		this.finished = finished;
	}
}
