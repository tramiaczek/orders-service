#!/bin/bash

ORDERS_SERVICE_PATH=/usr/local/orders-service/
PID_FILE=$ORDERS_SERVICE_PATH/app_generator.pid

JAVA_HOME=/usr/local/jdk1.8.0_111
PATH=$PATH:$JAVA_HOME/bin/
export PATH

# Returns PID of the daemon proces if it is running
# Returns 0 if the process is not running
get_pid()
{
    PID=0
    
    if [ -r ${PID_FILE} ]; then
        PID=`cat ${PID_FILE}`

        if [ -n "`ps -p $PID | grep java`" ]; then
            return 1
        fi     
        
        PID=0
    fi
    
    return 0
}


cd $ORDERS_SERVICE_PATH


case $1 in
    'start')
        # Start App generator and write its process id to the PID_FILE
        get_pid

        if [ $PID -eq 0 ]; then
           cd $ORDERS_SERVICE_PATH
           nohup $JAVA_HOME/bin/java -Dlog.dir=$ORDERS_SERVICE_PATH/logs -jar order-service-0.0.1-SNAPSHOT.jar 2>&1 > logs/nohup.out &
           echo $! > ${PID_FILE}
           
           
           # check if process has been successfully started
           sleep 3
           get_pid
           
           if [ $PID -eq 0 ]; then
             echo "Starting Orders service failed!"
           else
             echo "Orders service started (PID: $PID)"  
           fi
           
        else
           echo "It seems that Orders service is already running (PID: $PID)"
        fi
        ;;


    'stop')
        # Stop Orders service
        get_pid

        if [ $PID -eq 0 ]; then
            echo "It seems that Orders service is already stopped"
        else
            kill -TERM $PID
            
            echo -n "Waiting for service stop"
            for (( i=0; i<20; i++ )); do
                echo -n "."
              	get_pid
              		
              	if [ $PID -eq 0 ]; then
                    break
              	fi
              	
              	sleep 1
            done


            # make sure it is stopped
            if [ $PID -ne 0 ]; then
            	kill $PID
            fi
            
	    echo
	    echo "Orders service stopped."
	fi
        ;; 
           
           
    'status')
        get_pid
        
	if [ $PID -eq 0 ]; then
	    echo "Orders service is not running";
	else
            echo "Orders service is running (PID: $PID)";
        fi
	;;
	
    *)
        echo "usage: $0 {start|stop|status}"
        ;;
esac

exit
