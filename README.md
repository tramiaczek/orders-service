# Budowanie i uruchamianie projektu orders-service PoC #

Do uruchomienia i zbudowania projektu konieczne jest [JDK 8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html) 
oraz [Maven 3.x](https://maven.apache.org/download.cgi) (który jest dystrybyowany wraz z kodem, więc nie ma konieczności, aby go ściągać.
Projekt został napisany przy użyciu frameworku Spring Boot.

### Wstępna konfiguracja ###

Po ściągnieciu projektu przy pomocy Git (git clone) 
lub bezpośrednio klikając na link ze [spakowanymi źródłami](https://bitbucket.org/tramiaczek/orders-service/downloads/).
Następnie konieczne jest zainstalowanie JDK 8 na środowisku, gdzie serwis będzie budowany i uruchamiany oraz ustawienie zmiennej 
środowiskowej **JAVA\_HOME** na katalog gdzie zainstalowano maszynę wirtualną. Ważne jest także dodanie katalogu: **JAVA\_HOME\bin** do ścieżki systemowej 
(np.: %Path% na Windows).

### Budowanie projektu ###

* Wchodzimy do katalogu, do którego zostały ściągnięte źródła projeku, np.: cd order-service\ i uruchamiamy w nim powłokę systemu (bash, CMD na Windows)
* W katalogu projektu uruchamiamy w linii poleceń (będąc w katalogu projektu): mvn clean package assembly:single
* Rozpocznie się ściąganie potrzebnych zależności z repozytoriów Maven w internecie. 
* Biblioteki będą potrzebne tylko do budowania serwisu, zostaną także włączone do pliku wynikowego.
* Projekt zbudował się, gdy w konsoli zobaczymy komunikat: BUILD SUCCESS oraz w podkatalogu **_target_** powstanie archiwum: **order-service-0.0.1-SNAPSHOT-bin.zip**

### Uruchamianie serwisu ###

* Na doecelowej maszynie (gdzie jest zainstalowana JDK 1.8) rozpakowujemy archiwum: **order-service-0.0.1-SNAPSHOT-bin.zip**
* Przechodzimy do katalogu: order-service-0.0.1-SNAPSHOT, gdzie znajdują się podkatalogi do uruchomienia serwisu:
    * katalog na logi: logs 
    * katalog z konfiguracją m.in. namiarami na bazę oraz testową bazę in-memory: config
    * przykładowy skrypt \*.sh, który można wykorzystać do uruchamiania serwisu na Linux/Unix
    * zbudowany plik z serwisem (uruchamialny \*.jar)
* Możemy uruchomić serwis w trybie demonstracyjnym, który bazuje na bazie H2 (w pamięci) inicjowanej z załączonych skryptów: config/schema-h2.sql, config/data-h2.sql
    * w tym celu uruchamiamy z linii poleceń: **java -Dspring.profiles.active=h2 -Dlogs.dir=logs -jar order-service-0.0.1-SNAPSHOT**
* Uruchomienie w trybie komunikacji z bazą MSSQL wymaga modyfikacji paramterów połączenia do bazy w pliku: config/application-mssql.properties oraz zmiany parametru uruchomieniowego:
    * **java -Dspring.profiles.active=mssql -Dlogs.dir=logs -jar order-service-0.0.1-SNAPSHOT**
* Po prawidłowym uruchomieniu serwis powinien nasłuchiwać ruchu HTTP na porcie 8080. Zmiana portu lub inna konfiguracja 
  związana z HTTPS odbywa się poprzez modyfikację/dodanie do plików config/application-{profile}.properties ustawień [Spring Boot](https://docs.spring.io/spring-boot/docs/current/reference/html/common-application-properties.html), 
  sekcja Embedded Server.

### Korzystanie z serwisu ###

* Serwis obsługuje komunikację zgodną z REST i aktualnie udostępnia dwie metody:
    * pobranie listy zleceń (w trybie H2 inicjowane skryptem: config/data-h2.sql)
    * aktualizacja pola 'finished' dla zleceń o podanych ID
* Aby pobranie listy zleceń możemy wysłać z dowolnego klienta HTTP zapytanie GET na adres: http://localhost:8080/api/orders/. 
  W odpowiedzi dostaniemy dokument JSON zawierający informacje o zleceniach z bazy:
    * Przykład żądania (HTTP):  
    GET http://localhost:8080/api/orders/ HTTP/1.1
    * Przykład odpowiedzi:  
    [{"id":1,"name":"Order 1","description":"Important order","finished":false},{"id":2,"name":"Order 2","description":"Important order","finished":false}]
* Aby zaktualizować status zlecenia wymagane jest wysłanie zapytania PUT na adres: http://localhost:8080/api/orders/. 
  Ważne jest, aby w zapytaniu ustawić nagłówek HTTP: **Content-Type** na wartość: **application/json**, 
  czym sygnalizujemy, że dane wejściowe będą w tym formacie.
    * Przykład żądania:  
    PUT http://localhost:8080/api/orders/ HTTP/1.1  
    Content-Type: application/json  
    [{"id":"1","finished":true},{"id":"4","finished":true}]  
    * Przykład odpowiedzi:
    HTTP 200
* Serwisy oczywiście można wołać z poziomu aplikacji Android, np.: korzystając z biblioteki [HttpClient](http://hc.apache.org/httpclient-3.x/):
```java
try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
            HttpPut httpPut = new HttpPut("http://localhost:8080/api/orders/");
            httpPut.setEntity(new StringEntity("[{\"id\":\"1\",\"finished\":true},{\"id\":\"4\",\"finished\":true}]"));
            httpPut.setContentType("application/json");
            System.out.println("Executing request " + httpPut.getRequestLine());

            // Create a custom response handler
            ResponseHandler<String> responseHandler = response -> {
                int status = response.getStatusLine().getStatusCode();
                if (status >= 200 && status < 300) {
                    HttpEntity entity = response.getEntity();
                    return entity != null ? EntityUtils.toString(entity) : null;
                } else {
                    throw new ClientProtocolException("Unexpected response status: " + status);
                }
            };
            String responseBody = httpclient.execute(httpPut, responseHandler);
            System.out.println("----------------------------------------");
            System.out.println(responseBody);
        }
```

### Kilka słów o implementacji ###
* Najważniejsze elementy kodu:
    * com\demo\orders\services\OrdersService.java - logik biznesowa pobierająca/zapisująca zlecenia w bazie
    * com\demo\orders\controllers\OrdersController.java - kontroler Spring'a wystawiający w formie REST logikę biznesową,
    * com\demo\orders\dto - obiekty mapowane z/na JSON